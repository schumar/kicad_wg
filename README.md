# Hardware for WG Sensor Platform

_Work In Progress_

A "Controller" board, with Bluepill and LoRa-module, which is connected via a
(long) 9/10-pin cable to a "Sensorboard", to which a bunch of sensors can be
connected.

A prototype has already been built, showing multiple possibilities for
improvement `:)`

## Cable

Current colorcode/pinout (some slight errors in there...)
```
PCB        Plug        Plug       PCB
  1   braun 1 braun     1 braun   1
  2     rot 2 rot       2 rot     2
  3  orange 3 orng+schw 3 orange  3     GND, 2 wires
  4    gelb 4 gelb      4 gelb    4
  5   gruen 5 gruen     5 gruen   5
  6    blau 6 blau      6 blau    6
  7 violett 7 hellblau  7 violett 7     colors!
  9   gruen 8 grau      8 gruen   9     colors! number mismatch!
  8 schwarz 9 weisz     9 schwarz 8     colors! number mismatch!
```

## Bluepill

![bluepill](bluepill.png)

Thanks to [Rasmus Friis Kjeldsen](http://reblag.dk/stm32)!

